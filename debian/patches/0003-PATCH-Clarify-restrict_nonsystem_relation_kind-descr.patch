From 6f2525c775f28600038d8f06d361c9b059dad686 Mon Sep 17 00:00:00 2001
From: Masahiko Sawada <msawada@postgresql.org>
Date: Fri, 30 Aug 2024 15:05:55 -0700
Subject: [PATCH] Clarify restrict_nonsystem_relation_kind description.

This change improves the description of the
restrict_nonsystem_relation_kind parameter in guc_table.c and the
documentation for better clarity.

Backpatch to 12, where this GUC parameter was introduced.

Reviewed-by: Peter Eisentraut
Discussion: https://postgr.es/m/6a96f1af-22b4-4a80-8161-1f26606b9ee2%40eisentraut.org
Backpatch-through: 12

origin: https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=6f2525c775f28600038d8f06d361c9b059dad686
---
 doc/src/sgml/config.sgml     | 6 +++---
 src/backend/utils/misc/guc.c | 2 +-
 2 files changed, 4 insertions(+), 4 deletions(-)

diff --git a/doc/src/sgml/config.sgml b/doc/src/sgml/config.sgml
index 0d545d0401..38f09327d8 100644
--- a/doc/src/sgml/config.sgml
+++ b/doc/src/sgml/config.sgml
@@ -8250,9 +8250,9 @@ SET XML OPTION { DOCUMENT | CONTENT };
      </term>
      <listitem>
       <para>
-       This variable specifies relation kind to which access is restricted.
-       It contains a comma-separated list of relation kind.  Currently, the
-       supported relation kinds are <literal>view</literal> and
+       Set relation kinds for which access to non-system relations is prohibited.
+       The value takes the form of a comma-separated list of relation kinds.
+       Currently, the supported relation kinds are <literal>view</literal> and
        <literal>foreign-table</literal>.
       </para>
      </listitem>
diff --git a/src/backend/utils/misc/guc.c b/src/backend/utils/misc/guc.c
index 23f5166341..41f46491ed 100644
--- a/src/backend/utils/misc/guc.c
+++ b/src/backend/utils/misc/guc.c
@@ -4214,7 +4214,7 @@ static struct config_string ConfigureNamesString[] =
 
 	{
 		{"restrict_nonsystem_relation_kind", PGC_USERSET, CLIENT_CONN_STATEMENT,
-			gettext_noop("Sets relation kinds of non-system relation to restrict use"),
+			gettext_noop("Prohibits access to non-system relations of specified kinds."),
 			NULL,
 			GUC_LIST_INPUT | GUC_NOT_IN_SAMPLE
 		},
-- 
2.39.2

